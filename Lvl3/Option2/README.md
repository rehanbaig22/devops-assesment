Create ECS Cluster using Terraform

Prerequisite:

Bucket is created in the right region for Terraform state file to go
Front end is ready to expose the application sitting in private subnet of ECs

Actions:

use appropiate aws config profile and Export as AWS_PROFILE Environment variable to target the right account
from the directory /ecstest/terraform-aws-ecs/ecs-stack do Terraform init, Terraform plan and Terraform Apply


CICD Pipeline

Prerequisite:
AWS Credential plugin installed in Jenkins 
AWS Access key and secret credential added in jenkins which points to the AWS Idenity authorize to access resources

Build process:

Go to Jenkins and created a pipeline project, select pipeline scritp from SCM under PIPELINE 

Pass the repoistory url this code sitting on
use AWS Credentials plugin which defind in prereq section
check on "GitHub hook trigger for GITScm polling"

